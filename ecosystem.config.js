const APP_NAME = require('./package.json').name;
module.exports = {
  apps: [{
    name: APP_NAME,
    script: "src/server/index.js",
    env: {
      "DEBUG": APP_NAME + ":*",
      "PORT": 8080
    }
  }]
}