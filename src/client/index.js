import React from "react";
import ReactDOM from "react-dom";

import "./res/index.css";

class App extends React.Component {
  render() {
    return (
      <div>
        <h1 id="head">Base Project!</h1>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
