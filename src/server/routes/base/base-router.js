const express = require('express')

class BaseRouter {
	constructor() {
		this.router = express.Router()
		this.init()
	}
}

module.exports = BaseRouter