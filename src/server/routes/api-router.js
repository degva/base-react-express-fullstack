const BaseRouter = require('./base/base-router');
//debug
const debug = require('debug')('knxapi:apiRouter')

class ApiRouter extends BaseRouter {
	init() {
		this.router.get('', (_, res) => res.send('Hey'))
	}
}

module.exports = new ApiRouter().router;