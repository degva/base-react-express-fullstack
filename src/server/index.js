const http = require('http')
//Para desarrollo
const debug = require('debug')('knxapi:index')
const app = require('./App')
//Define el puerto
const port = normalizePort(process.env.PORT || 8080)	
app.set('port', port)
//Crea el servidor
const server = http.createServer(app)
server.listen(port)
server.on('error', onError)
server.on('listening', onListening)

function onError(error) {
	switch(error.code) {
		case 'EACCES':
			debug('Necesitas privilegios elevados')
			process.exit(1)
			break
		case 'EADDRINUSE':
			debug('Ya esta en uso')
			provess.exit(2)
			break
		default:
			throw error
	}
}

function onListening() {
	debug("Holi, estamos conectados :)")
}

function normalizePort(val) {
	let port = (typeof val === 'string') ? parseInt(val, 10) : val
	if (isNaN(port))
		return val
	else if (port >= 0)
		return port
	else
		return false
}
