//Express: Http, body-parser: parsear body  node's fault
const express = require('express')
const bodyParser = require('body-parser')

const logger = require('morgan')
const debug = require('debug')('knxapi:App.js')

const apiRouter = require('./routes/api-router.js')

class App {
	constructor() {
		debug('Inicializando api...');
		this.express = express();
		this.middleware()
		this.routes()
	}

	middleware() {
		this.express.use(logger('dev'))
		this.express.use(bodyParser.json())
		this.express.use(bodyParser.urlencoded({
			extended: true
		}))
		this.express.use(express.static("dist"));
	}

	routes() {
		this.express.use('/api', apiRouter)
	}
}

module.exports = new App().express